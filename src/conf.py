# coding=utf-8
import yaml, sys, os
#from ruamel.yaml import YAML


#Here the global variables corresponding to the path to YAML files
PRESETS_CONFIG_FILE = "presets.yaml"

DEFAULT_PATH = ""
DEFAULT_MAX_ROWS = 50000

#Nothing to do here. The class constructor receives the path where
#YAML file is located. Loads the file and using the exec() function
#convert each dictionary key to a class variable
class Conf():

    def __init__(self,config_file):
        #self.yaml = YAML()
        self.loadYamlFile(config_file)
        self.file_name_yml = config_file

    def loadYamlFile(self,yaml_file):
        print ("INFO: Cargando valores por defecto: {}".format(yaml_file))
        if( os.path.isfile( yaml_file ) ):
            fd = open (yaml_file, "r", encoding='utf-8-sig')
            self.data = yaml.load( fd , Loader=yaml.FullLoader )
            if(type(self.data) == list):    #If the yaml file just contains a list, puts the values into a list
                self.l_values = self.data
            else:
                for key in self.data.keys():
                    if( type(self.data[key]) == str ):
                        s = "self.{} = \"{}\"".format(key,self.data[key])
                    else:
                        s = "self.{} = {}".format(key,self.data[key])
                    exec( s )
        else:
            print ("ERROR: fichero no encontrado! Usando valores de fábrica.")
            self.work_dir = DEFAULT_PATH
            self.max_rows = DEFAULT_MAX_ROWS
    
    #ruamel.yaml.YAML not working when compiling. The following code not executed yet.
    def saveYaml( self ):
        for key in self.data.keys():
            s = "self.data[\"{}\"] = self.{}".format(key,key)
            exec( s )        
        with open(self.file_name_yml, 'w', encoding='utf-8-sig') as outfile:
            self.yaml.dump(self.data, outfile)        
        

if (__name__ == "__main__"):
    conf  = Conf( PRESETS_CONFIG_FILE )


