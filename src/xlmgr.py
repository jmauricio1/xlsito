import xlwt, xlrd, xlutils
from xlutils.copy import copy
from xlwt import Workbook, Formula
import numpy as np

class xlmgr( ):

	def __init__( self , xls_file_name ):
		self.xls_file_name  = xls_file_name
		self.wb			    = Workbook( encoding="UTF-8" )
		self.dict_sheets    = {}
		self.dict_precision = {}
		self.sheet_ptr	    = None

	def sheet_count( self ):
		return len( self.dict_sheets )

	def sheet_exist( self , sheet_name ):
		return (sheet_name in self.dict_sheets)

	def sheet_add( self , sheet_name , precision=None ):
		self.dict_sheets[sheet_name] = self.wb.add_sheet ( sheet_name , cell_overwrite_ok=True )
		self.dict_precision[sheet_name] = precision
		self.sheet_ptr = self.dict_sheets[sheet_name]
		return self.dict_sheets[sheet_name]

	def sheet_del( self , sheet_name ):
		if( type(sheet_name) == list ):
			sheets_to_delete = sheet_name
		else:
			sheets_to_delete = [ sheet_name ]
		new_wb = Workbook()
		for sh in self.dict_sheets():
			if( sh not in sheets_to_delete ):
				self.new_wb.add_sheet ( sh , cell_overwrite_ok=True )
			else:
				del self.dict_sheets[ sh ]
		wb = new_wb

	def sheet_goto( self , sheet_name , precision=None ):
		if( self.sheet_exist( sheet_name ) ):
			self.sheet_ptr = self.dict_sheets[sheet_name]
			self.dict_precision[sheet_name] = precision
		else:
			self.sheet_ptr = self.sheet_add( sheet_name , precision )

	def sheet_print( self ):
		for i,sheet in enumerate(dict_sheets.keys()):
			print("{:}: '{}' ".format(i,sheet))

	def sheet_get( self , sheet_name="" ):
		#self.wb.get_sheet(sheet_name) --> this method is natively implemented
		if( sheet_name == "" ):
			return self.sheet_ptr
		else:
			return self.dict_sheets[sheet_name]

	def sheet_ptr_check( self ):
		if( self.sheet_ptr == None ):
			print("FATAL: Create a sheet before writing. Use sheet_goto() or sheet_add() functions first!")
			return(1)

	def sheet_set_precision( self , sheet_name , precision ):
		if( self.sheet_exist( sheet_name ) ):
			self.dict_precision = precision

	def is_empty( self ):
		return (self.sheet_count() == 0)

	def load( self ):
		wb_read = xlrd.open_workbook(self.xls_file_name)
		self.wb = copy( wb_read )
		for i,sheet_name in enumerate(wb_read.sheet_names()):
			self.sheet_ptr = self.wb.get_sheet(i)
			self.dict_sheets[ self.sheet_ptr.name ] = self.sheet_ptr
			self.dict_precision[ self.sheet_ptr.name ] = None

	def save( self ):
		if( self.is_empty() ):
			print( "WARNING: the workbook is empty and cannot be saved as '{:}'".format(self.xls_file_name) )
		else:
			self.wb.save( self.xls_file_name )

	#E.g.: data: [0,1,2,3,4], fmt_str: "CH {:}" --> ["CH 0","CH 1","CH 2","CH 3","CH 4"]
	def write_header_str( self , off_xy , orientation_h , data , fmt_str ):
		self.sheet_ptr_check()
		h = int(orientation_h)
		v = 1-h
		for idx,value in enumerate( data ):
			self.sheet_ptr.write(off_xy[0]+v*idx, off_xy[1]+h*idx, fmt_str.format(value) )

	def write_matrix( self , off_xy , m_data ):
		self.sheet_ptr_check()
		for idx, value in np.ndenumerate(m_data):
			self.write( (off_xy[0]+idx[0],off_xy[1]+idx[1]) , value )

	def write( self , off_xy , value ):
		self.sheet_ptr_check()
		fmt = self.get_precision_fmt( )
		if( (fmt != "") and isinstance(value,float) ):
			self.sheet_ptr.write( off_xy[0] , off_xy[1] , float(fmt%(value)) )
		else:
			self.sheet_ptr.write( off_xy[0] , off_xy[1] , value )

	def get_precision_fmt( self ):
		prec = self.dict_precision[ self.sheet_ptr.name ]
		if( prec==None ):
			return ""
		else:
			return "%.{:}f".format( self.dict_precision[ self.sheet_ptr.name ] )
