import tkinter as tk
from tkinter.filedialog import askopenfile 
from tkinter import messagebox
from tkinter.scrolledtext import ScrolledText
from tkinter import ttk
import os.path

#GUI constants:
WIN_W = 800
WIN_H = 555

SW_MAJOR = 0
SW_MINOR = 5

ICON_PATH = "./ico/yama.ico"

class Gui():

    def __init__( self , conf , split ):
        self.conf = conf
        self.split = split
        self.window = tk.Tk()
        self.window.title("xlsito v{:d}.{:d}".format(SW_MAJOR,SW_MINOR))
        self.window.minsize( WIN_W , WIN_H )
        self.canvas = tk.Canvas(self.window, width = WIN_W, height = WIN_H)
        self.window.grid_columnconfigure(0, weight=1)
        self.window.grid_rowconfigure(0, weight=1)
        self.window.resizable(False, False)
        try:
            self.window.iconbitmap( ICON_PATH )
        except:
            print("ERROR: Window Icon '{:s}' not found!".format(ICON_PATH))
        self.create_widgets()
        
    def create_widgets( self ):
        #File location Label:
        label = tk.Label(self.window, text= 'Ubicación del fichero Excel de entrada:')
        label.place(x=15,y=15)
        #Work directory Text Entry:
        self.sv_xls_file_in = tk.StringVar()
        self.txt_xls_file_in = ttk.Entry(self.window, width = 113, textvariable=self.sv_xls_file_in,
                                         validate="key", validatecommand=self.validate_path)
        self.txt_xls_file_in.place(x=15,y=40)
        #Browse button:
        self.b_browse = ttk.Button(text="Examinar...",command=self.browse_file)
        self.b_browse.place(x=715,y=39)
        #Max rows Label:
        label = tk.Label(self.window, text= 'Filas por fichero:')
        label.place(x=15,y=70)        
        #Max rows Text Entry:
        self.sv_max_rows = tk.StringVar()
        self.txt_max_rows = ttk.Entry(self.window, width = 15, textvariable=self.sv_max_rows)
        self.txt_max_rows.place(x=15,y=95)
        self.sv_max_rows.set( self.conf.max_rows )
        #Split button:
        self.b_split = ttk.Button(text="Partir!",command=self.check_and_split)
        self.b_split.place(x=120,y=95)
        #File location Label:
        label = tk.Label(self.window, text= 'Consola de eventos:')
        label.place(x=15,y=125)        
        #Console text:
        self.txt_console = ScrolledText(self.window,wrap=tk.WORD,width = 110, height = 25, font = ("Courier New",8) )
        self.txt_console.pack( side = "bottom" , padx=15, pady=50, expand=False )
        #Console Buttons:
        self.b_copy = ttk.Button(text="CTRL+C",command=self.copy_console)
        self.b_copy.place(x=640,y=110)        
        self.b_clear = ttk.Button(text="Borrar",command=self.clear_console)
        self.b_clear.place(x=730,y=110)        
        #Progress Bar:
        self.progress_bar = ttk.Progressbar( self.window )
        self.progress_bar.place(x=15, y=520, width=790)
        
    def browse_file( self ):
        title = "Selecciona el fiechero Excel de entrada..."
        file_name = askopenfile(initialdir=self.conf.work_dir, title=title, mode ='r', filetypes =[('*.xls', '*.xlsx')]) 
        self.sv_xls_file_in.set( "" if file_name is None else file_name.name )
      
    def validate_path(self):
        pass
        
    def check_and_split( self ):
        xls_file_in = self.sv_xls_file_in.get()
        if( os.path.isfile( xls_file_in ) == 0 ):
            messagebox.showerror("Error", "La ubicación del fichero Excel de entrada es incorrecta!")
            return
        try:
            max_rows = int( self.txt_max_rows.get() )
        except ValueError:            
            messagebox.showerror("Error: Filas por fichero", "El campo deber ser un número entero.")
            return
        if( max_rows < 1 ):
            messagebox.showerror("Error: Filas por fichero", "El campo deber ser un número mayor que 0.")
            return

        self.conf.max_rows = max_rows
        self.conf.work_dir = os.path.dirname( xls_file_in )
        #self.conf.saveYaml()        
        self.split( xls_file_in , max_rows , self )
        
    def print( self , msg ):
        self.txt_console.insert( tk.INSERT , msg+"\n" )
        self.txt_console.see(tk.END)
        
    def clear_console( self ):
        self.txt_console.delete("0.0","end")

    def copy_console( self ):
        self.window.clipboard_append( self.txt_console.get("1.0",tk.END) )
        self.progress_bar.step(100)
        
    def run( self ):    
        self.window.mainloop()
        
    def progress_bar_init( self , max_val ):
        self.progress_bar["value"] = 0
        self.progress_max = max_val
        self.window.update()
        
    def progress_bar_set( self , val ):
        self.progress_bar["value"] = 20 + 80 * val / self.progress_max
        self.window.update()

    def progress_bar_update( self , update_val ):
        self.progress_bar["value"] = update_val
        self.window.update()        