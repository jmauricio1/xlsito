#!/usr/bin/python
from sys import path
path.append('./src')
from gui     import *
from conf    import *
from xlwt import Workbook
import xlrd
from shutil import copyfile
from tempfile import gettempdir
import os.path

def chunks(l, n):
    """Yield successive n-sized chunks from l."""
    for i in range(0, len(l), n):
        yield l[i:i + n]

def get_filename_out( file_name_in , i_chunk , n_chunks ):
    file_no_ext = file_name_in.split(".")[-2]
    return( file_no_ext + ".parte{:d}de{:d}.xls".format(i_chunk+1,n_chunks) )     

def make_xls_local( xls_file_in ):
    temp_file = gettempdir() + "\\" + os.path.basename(xls_file_in)
    print("INFO:   - Copia local en {:s}".format(temp_file))
    try:
        copyfile( xls_file_in , temp_file )
        return temp_file
    except:
        print("ERROR: Imposible hacer la copia local!")
        return xls_file_in
        
def move_output_files( xls_file_out , work_dir ):
    if( os.path.dirname(xls_file_out) == gettempdir() ):
        xls_file_out_orig = work_dir + "\\" + os.path.basename(xls_file_out)
        try:
            if( os.path.exists(xls_file_out_orig) ):
                os.remove(xls_file_out_orig)
                
            copyfile( xls_file_out , xls_file_out_orig )
            os.remove(xls_file_out)
            
            xls_file_out = xls_file_out_orig
        except:
            print("ERROR: Imposible mover el fichero temporal {:s} al directorio de trabajo: {:s}".format(xls_file_out,work_dir))
            return
        
    print("INFO: Fichero '{:s}' guardado.".format(xls_file_out))    


def remove_tmp_file( filename ):
    if( os.path.exists(filename) ):
        os.remove(filename) 

def split( xls_file_in , max_rows , gui ):
    print("INFO: Cargando el fichero '{:s}'...".format(xls_file_in,max_rows))
    gui.progress_bar_update(0)
    
    #Original workspace directory:
    work_dir = os.path.dirname(xls_file_in)
    #Local copy of the input file.
    xls_file_in = make_xls_local(xls_file_in )
    gui.progress_bar_update(10)
    
    workbook = xlrd.open_workbook( xls_file_in )
    sheet = workbook.sheet_by_index(0)
    
    _chunks = list(chunks(range(0, sheet.nrows), max_rows))
    print("INFO:   - Número de filas : {:d}".format(sheet.nrows))
    print("INFO:   - Número de partes: {:d}".format(len(_chunks)))
    gui.progress_bar_init( sheet.nrows )
    gui.progress_bar_update(20)
    row_progress = 0

    for i,_chunk in enumerate(_chunks):
        wb = Workbook()
        ws = wb.add_sheet('Hoja {:d} de {:d}'.format(i+1,len(_chunks)) )
        
        rows = [sheet.row_values(row, 0) for row in _chunk]
    
        for rowx, row_values in enumerate(rows):
            row_progress += 1
            if( row_progress % 1000 == 0 ):
                gui.progress_bar_set( row_progress )
                
            for colx, cell_value in enumerate(row_values):
                ws.write(rowx, colx, cell_value)

        xls_file_out = get_filename_out( xls_file_in , i , len(_chunks) )
        wb.save( xls_file_out )    
        move_output_files( xls_file_out , work_dir )
        remove_tmp_file( xls_file_in )
        
    gui.progress_bar_update(100)
    
    

if (__name__ == "__main__"):
    conf = Conf( PRESETS_CONFIG_FILE )
    gui = Gui( conf , split )
    #Overwrite print method: log messages appearing in the GUI console:
    print = gui.print
    gui.run()
    
    

    
