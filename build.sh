PATH=$PATH:"/c/ProgramData/Anaconda3/Scripts"

SW_MAJOR=$(cat src/gui.py | grep "SW_MAJOR = " | awk '{print $3}')
SW_MINOR=$(cat src/gui.py | grep "SW_MINOR = " | awk '{print $3}')

SW_VERSION="$SW_MAJOR.$SW_MINOR"
SW_NAME="xlsito_v$SW_VERSION"

echo "INFO: xlsito version $SW_VERSION"
echo "INFO: Removing any previous compilation output..."
echo "INFO: rm dist/$SW_NAME -rf"
rm dist/$SW_NAME -rf

SPEC_FILE="./spec/$SW_NAME.spec"
echo "INFO: Customizing SPEC file '$SPEC_FILE'..."
cp ./spec/xlsito.base.spec $SPEC_FILE

#Replaces dummy APP_NAME by the current application name:
sed -i -e "s/APP_NAME/$SW_NAME/g" $SPEC_FILE

pyinstaller $SPEC_FILE

echo "INFO: Creating 7z Self-Extraction file (SFX)..."
cd dist
echo "7z.exe a \"Extraer_$SW_NAME.exe\" $SW_NAME -mx9 -sfx7z.sfx"
7z.exe a "Extraer_$SW_NAME.exe" $SW_NAME -mx9 -sfx7z.sfx
cd ..
echo "INFO: Removing uncompressed files..."
echo "INFO: rm dist/$SW_NAME -rf"
rm dist/$SW_NAME -rf

echo "INFO: Done!"